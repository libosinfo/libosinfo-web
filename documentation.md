## Documentation

* To learn about using Libosinfo try the [API guide](api.html)
* To find out more about our project build OS platform support statement please read [Build platforms](build_platforms.html)
